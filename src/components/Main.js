import React from 'react'
import { Switch, Route } from 'react-router-dom'
import ImportForm from './ImportForm'
import Table from './Table'
import Export from './Export'
import Home from './Home'

const Main = () => {
    return <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/import' component={ImportForm} />
        <Route path='/table' component={Table} />
        <Route path='/export' component={Export} />
    </Switch>
}

export default Main