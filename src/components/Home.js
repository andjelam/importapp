import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles({
  root: {
    minWidth: 275,

  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 5,
  },
  content:{
    fontSize:20
  },
  elements:{
    display:'inline-block'

  }
});

const Home = () =>{
  const classes = useStyles();
  return (
    <div className="elements" justifyContent="center" >
        <h1 align="center" > Importing and Exporting Excel files</h1>
         <p>
         <Card className={classes.root}>
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                About
                </Typography>
                <Typography className={classes.content}  component="h2">
                Import/Export app makes it easy to store, share and access your files
                 from just about anywhere.<br/>
                The applicaton not only allows users to upload excel files, <br/>
                but also lets you to download stored data in seconds. <br/>
                Uploaded files are stored in list 
                 with upload date and name for each of the file. 
                </Typography>
            </CardContent>
            <img size="small" align="right" alt="img" src={process.env.PUBLIC_URL + '/red.png'} />
            <CardActions>
                <Button size="small">Learn More</Button>
            </CardActions>
        </Card>
      </p> 
     
    </div>
     );
}

export default Home