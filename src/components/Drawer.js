import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import InputIcon from '@material-ui/icons/Input';
import ViewListIcon from '@material-ui/icons/ViewList';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import Paper from '@material-ui/core/Paper';
import {Link} from 'react-router-dom'
import HomeIcon from '@material-ui/icons/Home';
import Main from './Main';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ListItemText from '@material-ui/core/ListItemText';



const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      align: 'center',
      ...theme.typography.button,
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      padding: '0 30px',
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar:theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
    

  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  paper: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      marginLeft: -20,
      marginTop: -20,
      height:"60vh",
      width: "180vh"
    },
  },
  link: {
    ...theme.typography.button,
    backgroundColor: theme.palette.background.paper,
    color:"secundary",
  }
}));


function ResponsiveDrawer (props) {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
 


  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider/>
      <List>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <HomeIcon/>
              </Avatar>
            </ListItemAvatar>
            <ListItemText>
              <Link value="home" color="primary" style={{ textDecoration: 'none' }} className={classes.link} to='/' variant="body2"> HOME </Link> 
            </ListItemText>
          </ListItem>
          <Divider variant="inset" />
          <ListItem>  
              <ListItemAvatar> 
                <Avatar>
                  <InputIcon />
                </Avatar> 
              </ListItemAvatar>  
              <ListItemText>
                <Link style={{ textDecoration: 'none' }} className={classes.link} to='/import'> IMPORT</Link>
              </ListItemText>
          </ListItem>
          <Divider variant="inset"/>
          <ListItem> 
            <ListItemAvatar>
              <Avatar>
                <ViewListIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText>
              <Link value="table" style={{ textDecoration: 'none' }} className={classes.link} to ='/table'>LIST</Link>
            </ListItemText>
          </ListItem>
          <Divider variant="inset"/>
          <ListItem>
            <ListItemAvatar>
                <Avatar>
                  <ArrowRightAltIcon />
                </Avatar>
            </ListItemAvatar>
            <ListItemText>
               <Link value="export" style={{ textDecoration: 'none' }} className={classes.link} to ='/export'>EXPORT</Link> 
            </ListItemText>
          </ListItem>
          <Divider/>
      </List>   
     
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar} align="center"> 
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap align="center">
            <CloudUploadIcon/> Import/Export
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <div className={classes.paper}>
       <Paper>
        <Main />            
       </Paper>
   
       </div>
      </main>
    </div>
  );
}

ResponsiveDrawer.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  container: PropTypes.instanceOf(typeof Element === 'undefined' ? Object : Element),
};

export default ResponsiveDrawer;
