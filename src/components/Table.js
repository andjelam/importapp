import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {connect} from 'react-redux';

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

const SimpleTable = (props) => {
  const classes = useStyles();
  const items=props;

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="left" component="th" scope="row">ID number</StyledTableCell>
            <StyledTableCell align="left" component="th" scope="row">Name</StyledTableCell>
            <StyledTableCell align="left" component="th" scope="row">Date</StyledTableCell>
            <StyledTableCell align="left">Size&nbsp;(bytes)</StyledTableCell>
            <StyledTableCell align="left" component="th" scope="row">File</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {items.items.map(item=> (
          <StyledTableRow key={item.id}>
            <StyledTableCell align="left">{item.id}</StyledTableCell> 
            <StyledTableCell align="left" component="th" scope="row"> 
            {item.name}
            </StyledTableCell>
           <StyledTableCell align="left">{item.newDate}</StyledTableCell>   
           <StyledTableCell align="left">{item.fileSize}</StyledTableCell>
           <StyledTableCell align="left">{item.fileName}</StyledTableCell>
          </StyledTableRow>
        ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
const mapStateToProps = (state) => ({
  items: state.items,

});
export default connect (
  mapStateToProps
)(SimpleTable)