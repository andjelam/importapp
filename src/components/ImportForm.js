import React, {useState, useRef} from 'react';
import { makeStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography'
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import {connect} from 'react-redux'
import {addItemAction} from '../redux/redux'
import { uuid } from 'uuidv4'
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({

    root: {
      display: 'flex',
      flexWrap: 'wrap',
      fontFamily: "comic-sans"
      
    },
    textFields: {
      marginTop: "10vh",
      marginLeft: "10vh",
      marginRight: theme.spacing(1), 
      display: 'block',
      color:'secondary',
    },
    btn: {
        '& > *': {
          margin: theme.spacing(10),
          marginTop: "20vh",
          display: 'inline-block',
          background: theme.palette.action.selected, 
          fontFamily: '"Comic Sans"',
          borderRadius: "12px",
          size: "large",
          spacing: "20 vh"
        },
      },
      input: {
        display: 'none',
         
          '& > *': {
            margin: theme.spacing(20),
            marginTop: "20vh",
          },
      },
      label: {
        '& > *': {
          marginTop: theme.spacing(4),
        },
      },
     
      
  }));

  //initial state for the store
    const initialState = {
      isLoading: false,
      fileName: "",
      filePreview: null,
      fileSize: 0
    };
    const  ImportForm = (props) => {
        const classes = useStyles();
        const [name,setName]=useState();
        const [newDate, setDate]=useState();
        const [state, setState] = useState(initialState);
        const uploadInputEl = useRef(null);
        const confirmUpload = ()=> {
          if (name!=null && newDate!=null && state!==initialState) {
            return alert (" Upload successful!\n Name of the file: " + name +"\n Date of uploading: " +newDate+"\n Uploaded: "+fileName);
          } else if (name==null) {
            return alert("Name is not defined!");
          } else if (newDate==null){
            return alert("Date is not defined!");
          } else if (initialState){
            return alert ("File is not uploaded")
          }
          };

          const handleNameInput = e => {
            setName(e.target.value);
          }

          const handleDateInput = newDate => {
            setDate(newDate);
          }

          const handleUploadChange = async ({ target: { files } }) => {
            setState(prevState => ({ ...prevState, isLoading: true }));

        
            const file = files[0];
              await new Promise(res => {
                setTimeout(() => {
                  res(
                    setState(prevState => ({
                      ...prevState,
                      fileName: file.name,
                      filePreview: URL.createObjectURL(file),
                      fileSize: file.size,
                      isLoading: false
                    }))
                  );
                }, 2000);
              });
            };

            const uploadFile = async () => {
              if (state.imagePreview)
                setState(prevState => ({ ...prevState, isLoading: true }));
            };
      
            const { filePreview, fileName, fileSize} = state;
            
            const onSubmit = (event) =>{
              event.preventDefault();
              props.addItemAction({
                id:uuid(),
                name: name,
                date: newDate,
                fileSize: fileSize,
                fileName: fileName,
                complete: false
              });
              setName('');
            };

            return (
                <div className={classes.root} id="2">
                  <form className={classes.elements} onSubmit={onSubmit} id="testForm" >
                    <div>
                      <br/>
                        <TextField
                          id="txt"
                          name="field"
                          label="NAME"
                          style={{ margin: 16 }}
                          placeholder="Enter name of the file"
                          width="60vh"
                          margin="normal"
                          color="secondary"
                          type="text"
                          onChange={handleNameInput}
                          value={name}
                          InputLabelProps={{
                            shrink: true,
                            
                          }}
                        />
                        <br/>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <Grid container justify="space-around">
                            <KeyboardDatePicker
                              disableToolbar
                              variant="inline"
                              format="MM/dd/yyyy"
                              margin="normal"
                              id="date"
                              label="Date"
                              value={newDate}
                              onChange={handleDateInput}
                              KeyboardButtonProps={{
                                'aria-label': 'change date',
                              }}
                            />
                          </Grid>
                      </MuiPickersUtilsProvider>
                      </div>
                      <div className={classes.btn}>
                        <input
                          accept=".xls"
                          className={classes.input}
                          id="outlined-button-file"
                        multiple
                        type="file"
                        ref={uploadInputEl}
                        onChange={handleUploadChange}
                            />
                      <label htmlFor="outlined-button-file">
                      <label htmlFor="button-file"/>
                        <Button color="secondary" type="input"  variant="outlined"  component="span" onClick={uploadFile} startIcon={<CloudUploadIcon />}>
                          <Typography variant="title" color="inherit">
                            Upload
                          </Typography>
                          <div>
                          {filePreview ? (
                            <>
                              <p style={{ margin: "1px "}}>
                                ({fileName} - {(fileSize / 1024000).toFixed(2)}MB)
                              </p>
                            </>
                          ) : (
                            <Button/>
                          )}
                        </div>
                        </Button>
                        </label>
                      <label>
                        <Button color="secondary" type="submit"  variant="outlined" onClick={confirmUpload} startIcon={<CheckCircleIcon/>} >
                            Confirm
                        </Button>
                      </label>
                    </div>
                </form>
              </div>
              );
    }
const mapStateToProps= (state) =>({
  items: state.items
});

export default connect (
  mapStateToProps,
  {addItemAction}
)(ImportForm)


