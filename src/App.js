import React from 'react';
import Drawer from './components/Drawer';
import {Provider} from 'react-redux'
import {store} from './redux/redux'


const App=() =>(
    <Provider store={store}>
    <div className="App">
      <header className="App-header">
        <Drawer/>  
      </header>
      {/* <Main /> */}
    </div>
    </Provider>
  )
export default App;
