import {createStore, applyMiddleware,compose} from 'redux'
import thunk from 'redux-thunk';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const initialState = {
   items:[
            {
                id:'',
                name:'',
                date:'',
                fileSize:'',
                fileName:'',
                complete:true
            }
    ],
};
//Store
export const store = createStore (
    reducer,
    initialState,
    composeEnhancer(applyMiddleware(thunk))
);
//Reducer
function reducer(state,action){
    switch (action.type){
        case 'ADD_ITEM':
           return{
              ...state,
              items: [...state.items,action.payload]
                
           };
        default: 
        return state;
    }
}
//Actions 
export const addItemAction = (name,date,fileSize,fileName) => ({
    type: 'ADD_ITEM',
    payload: name,date,fileSize,fileName
  });
